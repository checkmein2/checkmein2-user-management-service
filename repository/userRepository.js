const User = require('../models/user');

const getUserById = userId => {
    User.findOne({ 'facebook.id': userId }, { _id: 0 }, (err, user) => {
        if (err) {
            return { error: err };
        }
        if (user) {
            return user.facebook;
        }
        return null;
    });
};

const updateUser = data => new Promise((resolve, reject) => {
    User.findOneAndUpdate(
        { id: data.id },
        data,
        {
            upsert: true,
            new: true,
            fields: { _id: 0 }
        },
        (err, user) => {
            if (err) {
                return reject(new Error(
                    `An error occured at retrieving user, err:${err}`
                ));
            }
            return resolve(user.toObject());
        }
    );
});

const getAllUsers = () => new Promise((resolve, reject) => {
    User.find({}, { _id: 0 }, (err, users) => {
        if (err) {
            return reject(new Error(
                `An error occured at retrieving users, err:${err}`
            ));
        }
        return resolve(users);
    });
});

module.exports = {
    getUserById,
    updateUser,
    getAllUsers
};
