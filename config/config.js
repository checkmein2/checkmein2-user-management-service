/* eslint-disable import/no-absolute-path*/
const secrets = require('/run/secrets/app_secrets');
/* eslint-enable import/no-absolute-path*/

const settings = {
    port: process.env.PORT || 5000,
    mongo_url: `mongodb://${process.env.MONGO_URL}:${process.env.MONGO_PORT}/userDataBase`,
    secretKey: secrets.SECRET_KEY || 'secret_key',
    adminUser: secrets.ADMIN_USER,
    adminPassword: secrets.ADMIN_PASSWORD
};

module.exports = Object.assign({}, { settings });
