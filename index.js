const express = require('express');
const session = require('express-session');
const cors = require('cors');
const jwt = require('express-jwt');
const bodyParser = require('body-parser');

const { settings } = require('./config/config');

const mongoose = require('mongoose');
mongoose.connect(settings.mongo_url);

const app = express();
const port = settings.port;

app.use(session({ secret: 'userLoginSecret' }));
app.use(cors());

app.use(bodyParser.json());
app.use(
    jwt({
        secret: settings.secretKey,
        credentialsRequired: false,
        getToken: function fromHeaderOrQuerystring(req) {
            if (req.headers.authorization !== '') {
                return req.headers.authorization;
            }
            return null;
        }
    }).unless({ path: ['/auth/facebookLogin', '/admin/login'] })
);
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);

// app.use('/admin/', (req, res) => {
//     const credentials = auth(req);
//     if (!credentials) {
//         res.statusCode = 401;
//         res.setHeader('WWW-Authenticate', 'Basic realm="Secure Area"');
//         res.end('Need some creds son');
//     } else if (credentials.name !== 'john' || credentials.pass !== 'secret') {
//         res.statusCode = 401;
//         res.setHeader('WWW-Authenticate', 'Basic realm="Secure Area"');
//         res.end('You shall not pass');
//     } else {
//         res.statusCode = 200;
//         res.end("It's you Brutus");
//     }
// });

require('./api/userRoutes.js')(app);
require('./api/health.js')(app);

app.listen(port, () => {
    /*eslint-disable*/
    console.log(`running on port ${port}`);
});
