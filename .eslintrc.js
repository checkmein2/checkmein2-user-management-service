// http://eslint.org/docs/user-guide/configuring
module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  extends: 'airbnb-base',
  // required to lint *.vue files
  plugins: [],
  // check if imports actually resolve
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': 'build/webpack.base.conf.js'
      }
    }
  },
  // add your custom rules here
  'rules': {
    // don't require .vue extension when importing
    'import/extensions': ['error', 'always', {
      'js': 'never',
      'vue': 'never'
    }],
    // allow optionalDependencies
    'import/no-extraneous-dependencies': ['error', {
      'optionalDependencies': ['test/unit/index.js']
    }],
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    'consistent-return': 0,
    'no-useless-escape': 0,
    'no-useless-return': 0,
    'no-restricted-properties': 0,
    'no-underscore-dangle': 0,
    'no-duplicate-imports': 0,
    'no-confusing-arrow': 0,
    'arrow-parens': 0,
    'import/no-unresolved': 0,
    'import/extensions': 0,
    'no-useless-constructor': 0,
    'comma-dangle': [ 2, 'never'],
    'indent': [ 2, 4, { 'SwitchCase': 1 }],
    'import/no-extraneous-dependencies': 0,
    'no-useless-rename': 0,
    'class-methods-use-this': 0,
    'no-prototype-builtins': 0,
    'no-mixed-operators': 0,
    'no-plusplus': 0,
    'operator-assignment': 0,
    'no-lonely-if': 0,
    'no-unexpected-multiline': 0,
    'space-unary-ops': 0,
    'import/prefer-default-export': 0,
    'import/newline-after-import': 0,
    },
}
