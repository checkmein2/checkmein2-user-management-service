const repository = require('../repository/userRepository');
const { settings } = require('../config/config');

// const https = require('https');
const jwt = require('jsonwebtoken');

module.exports = app => {
    app.post('/auth/facebookLogin', (req, res) => {
        repository.updateUser(req.body.profile).then(user => {
            res.json({ jwt: jwt.sign(user, settings.secretKey) });
        });
        // twitter
        //     const timestampt = new Date().getTime()/1000;
        //     const options = {
        //        hostname: 'api.twitter.com',
        //        path: (
        //            `/1.1/account/verify_credentials.json?
        //                    oauth_consumer_key=BCQdIWkyxpbxbvNxqDTT8omJE&
        //                    oauth_nonce=5694f8eac7a38c000&oauth_signature_method=HMAC-SHA1&
        //                    oauth_timestamp=${timestampt}&
        //                    oauth_token=844208955165413377-R1dFtrN3sVfi5diP1E5w5849c9DNIA0&
        //                    oauth_version=1.0&scope=email&
        //                    oauth_signature=%2FWfit%2BXQ4AnK8U%2BP7QzhWBDH4Wc%3D`
        //        )
        //    };
        //

        // facebook
        // const accessToken = `access_token=${token}`;
        // const options = {
        //     hostname: 'graph.facebook.com',
        //     path: (
        //         `/me?${accessToken}&suppress_response_codes=true`
        //     )
        // };


        // google
        // const requiredFields = 'fields=emails,displayName, name';
        // const options = {
        //     hostname: 'www.googleapis.com',
        //     path: (
        //         `/plus/v1/people/me?${requiredFields}&${accessToken}`
        //     )
        // };
        //    const httpCallBack = response => {
        //        let str = '';
        //        response.on('data', chunk => {
        //            str += chunk;
        //        });
        //        response.on('end', () => {
        //            const resp = JSON.parse(str);
        //            repository.updateUser(resp).then(user => {
        //                res.json({ jwt: jwt.sign(user, settings.secretKey) });
        //            });
        //        });
        //    };
        //    https.request(options, httpCallBack).end();
    });
    app.post('/auth/admin/login', (req, res) => {
        const { username, password } = req.body;
        if (username !== settings.adminUser || password !== settings.adminPassword) {
            res.status(401).send({
                succces: false,
                message: 'Anuthorized user'
            });
        }

        res.json({ jwt: jwt.sign({ id: 'admin_id', user_role: 'admin', email: 'some_admin_email', name: 'john' }, settings.secretKey) });
    });


    app.get('/auth/logout/:userId', () => {
        // probably nothing to do on logout
    });

    app.get('/users/currentUser', (req, res) => {
        res.json(req.user);
    });

    app.get('/users/:userId', (req, res) => {
        repository.getUserById(req.params.userId).then(user => {
            res.json(user);
        });
    });
    app.get('/users', (req, res) => {
        repository.getAllUsers().then(users => {
            res.json(users);
        });
    });

    app.post('/users/:userId', (req, res) => {
        repository.updateUser(req.body);
        res.status(200).send({
            succces: true
        });
    });
};
// update user data
// some sort of check by userId
