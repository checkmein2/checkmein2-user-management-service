const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    id: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    token: {
        type: String,
        default: ''
    },
    provider: {
        type: String
    },
    user_role: {
        type: String,
        enum: ['user', 'admin'],
        default: 'user'
    }
});

UserSchema.set('toObject', { versionKey: false });

module.exports = mongoose.model('User', UserSchema);
