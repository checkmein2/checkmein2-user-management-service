FROM node:7.7.3-alpine
# Create app directory
RUN mkdir -p /app
WORKDIR /app
# Install app dependencies
COPY package.json /app/
# Bundle app source
ENV PORT 5000
RUN yarn
COPY . /app
EXPOSE 5000
CMD [ "yarn", "start" ]
